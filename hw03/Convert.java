//Zack Elliott
//CSE 2 - 210
//Sept 16
//hw03 - calculating rainfall from hurricanes

import java.util.Scanner; //initializes scanner

public class Convert{
  //main method required for every java program
  public static void main(String[] args){
    
  Scanner myScanner = new Scanner(System.in);
  System.out.println("Enter the affected area in acres: "); //prompts the user for the hurricane-affected area in acres
  double affectedArea = myScanner.nextDouble(); //stores user input as a double
  System.out.println("Enter the rainfall in the affected area: "); //prompts the user for the
  double inchesRanifall = myScanner.nextDouble();
  
  double sqInchesPerMile = Math.pow((5280.0*12.0), 2.0); //equation to find the square inches per mile
  double gallonsPerMile = ((sqInchesPerMile*inchesRanifall)/231.0); //equation to find the number of gallons per square mile
  //231 is the number of square inches in 1 gallon
  double sqMilesAffected = affectedArea*0.0015625; //equation for total square miles affected by hurricane
  //0.0015625 is the number of square miles in 1 acre
  double totalGallons = gallonsPerMile*sqMilesAffected; //equation for total gallons of rain
  double gallonsInCubicMile = Math.pow(5280.0, 3.0) * 7.4805198; //equation to find gallons in a cubic mile
  double cubicMile = totalGallons/gallonsInCubicMile; //converting the amount of rainfall into cubic miles
    
  System.out.println(cubicMile + " cubic miles"); //output the final cubic mile value
    
  }
}

