//Zack Elliott
//CSE 2 - 210
//Sept 16
//hw03 - calculating rainfall from hurricanes

import java.util.Scanner; //initializes scanner

public class Pyramid{
  //main method required for every java program
  public static void main(String[] args){
  
  Scanner myScanner = new Scanner(System.in);
  System.out.println("The square side of the pyramid is (input length): "); //prompts the user for the square side length
  double sideLength = myScanner.nextDouble(); //stores user input as a double
  System.out.println("The height of the pyramind is (input height): "); //prompts the user for the height of the pyramid
  double height = myScanner.nextDouble(); //stores user input as a double
    
  double divideByThree = 3.0; //will be used for the voluma formula
  double areaOfBase = Math.pow(sideLength, 2); //formula for area of the base of the pyramid
  double pyramidVolume = areaOfBase*height/3.0; //formula for the volume of pyramid 
  
  System.out.println("The volume inside the pyramid: " + pyramidVolume + "."); //output of the final volume
 
  }
}

  