//Zack Elliott
//CSE 2
//hw09 - program 2 - removing elements

import java.util.Scanner;
import java.util.Random;

public class RemoveElements{
  public static void main(String [] arg){ //start of main method
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
 
      System.out.print("Enter the index ");
      index = scan.nextInt();
      if (index<0 || index>10){
        System.out.println("The index is not valid.");
      }
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
    
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
   }
    while(answer.equals("Y") || answer.equals("y"));
  } //end of main method
 
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  } //end of method
  
  public static int[] randomInput(){ //start of random input method
    int array1[]=new int[10]; //declaring array1
    for (int i=0; i<array1.length; i++){
      array1[i] = (int)(Math.random()*array1.length);
    }
    return array1;
  } //end of randomInput method
  
  
  public static int[] delete(int[] list, int pos){ //start of delete method
    //have to call in the array list and the integer pos to this method
    int array2[] = new int[list.length-1]; //declaring array2
    for (int i = 0; i<pos; i++){
      array2[i] = list[i];
    }
    //these for loops are used to print out an array without the value at "pos"
    for (int i = pos+1; i<list.length; i++){
      array2[i-1] = list[i];
    }
    return array2;
  } //end of delete method
  
  
  public static int[] remove(int[] list, int target){ //start of remove method
    int counter = 0;
    for (int i = 0; i<list.length; i++){
      if (list[i] == target){ //if any values in the array are equal to target, then they are going to be removed
        counter++;
      }
    }
    int[] array3 = new int[list.length-counter]; //declaring array3
    int filler = 0;
    for (int number: list){ //enhanced for loop
      if (number != target){
        array3[filler] = number;
        filler++;
      }
    }
    return array3;
    
  } //end of remove method
  
} //end of class
