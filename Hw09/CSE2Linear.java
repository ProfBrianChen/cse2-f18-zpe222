//Zack Elliott
//Nov 25, 2018
//CSE2
//HW09 - Searching - Program 1

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear{
  public static void main(String args[]){
    Scanner scanner = new Scanner(System.in);
    int grades[] = new int[15]; //declaring the grades array
    int i = 0;
    System.out.println("You will be entering 15 ascending integers for final grades in CSE2: ");
    for (i=0; i<grades.length; i++){
      System.out.println("Enter an integer: ");
      while (!scanner.hasNextInt()){ //checks if the user entered an integer or not
        System.out.println("You did not enter an integer, try again: ");
        String junk = scanner.nextLine();
      }
      grades[i] = scanner.nextInt();
      if (grades[i]<0 || grades[i]>100){ //checks if the user entered a valid grade
        System.out.println("Your integer is not between 1-100, try again: ");
        grades[i] = scanner.nextInt();
      }
      else if (i!=0 && grades[i] <= grades[i-1]){ //checks if the user is entering ascending grades
        System.out.println("Your integers are not in ascending order, try again: ");
        grades[i] = scanner.nextInt();
      }
    }
    binarySearch(grades); //calling the binary search method
    System.out.println("This is the shuffled array: ");
    shuffle(grades); //calling the shuffle method
    for (i =0; i<grades.length; i++){ //for loop to print out the shuffled array
      System.out.print(grades[i] + " ");
    }
    System.out.println();
    linearSearch(grades); //calling the linear search array
    
  } //end of main method
  
  public static void binarySearch(int[] grades){ //start of binary search method
    Scanner scanner = new Scanner(System.in);
    int first = 0;
    int counter = 0;
    int last = grades.length-1;
    int middle = (first+last)/2;
    System.out.println("Enter a grade to binary search for: ");
    int gradeSearch = scanner.nextInt();
    
    while (first<=last){
      counter++;
      if (grades[middle] < gradeSearch){
        first = middle+1;
      }
      else if (grades[middle] == gradeSearch){ //means that the value has been found by the binary search
        System.out.println(gradeSearch + " found.");
        break;
      }
      else {
        last = middle-1;
      }
      middle = (first+last)/2;
    }
    if (first > last){
      System.out.println(gradeSearch + " is not present in the list.");
    }
    System.out.println("The number of iterations was " + counter + " .");
    
    
  } //end of binary search method
    
  
  public static void shuffle(int[] grades){ //start of shuffle method
    for (int i = 0; i<grades.length; i++){
      int index = (int)(Math.random()*grades.length); //randomly generating an index for the grades
      int temp = grades[i];
      grades[i] = grades[index];
      grades[index] = temp;
    }
  } //end of shuffling method
  
  public static void linearSearch(int[] grades){ //start of linear search method
    int gradeSearch = 0;
    boolean found = false;
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter a grade to linear search for: ");
    gradeSearch = scanner.nextInt();
    for (int i = 0; i<grades.length; i++){ //linear search goes through each value and checks it
      if (grades[i]==gradeSearch){
        System.out.println(gradeSearch + " was found with " + (i+1) + " iterations.");
        found = true;
      }
    }
    if (found == false){
      System.out.println(gradeSearch + " was not found.");
    }
  } //end of linear search method
        
    
} //end of class
      