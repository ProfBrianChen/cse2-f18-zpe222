//Zack Elliott
//CSE2
//November 7, 2018
//Lab 8 - Arrays

import java.util.Random;
import java.util.Arrays;

public class lab08{
  public static void main(String[] args){
    int array1[] = new int[100]; //creating the first array
    int array2[] = new int[100]; //creating the second array
    int i =0;
    Random rand = new Random();
    
    
    //this for loop will generate random number from 0-99
    for (i=0; i<array1.length; i++){
      array1[i] = rand.nextInt(100);
    }
    
    for (int j=0; j<array1.length; j++){ //nested for loops to count occurences
      for (int k=0; k<array1.length; k++){
        if (array1[k] == j){
          array2[j] += 1;;
        }
      }
    }
    System.out.println("Array 1 holds the following integers: " + Arrays.toString(array1));
    //printing out array 1
    
    for (int z=0; z<array2.length; z++){ //for loop to print number of occurences
      System.out.println(z + " occurs " + array2[z] + " times.");
    }
   
      
  } //end of method
} //end of class