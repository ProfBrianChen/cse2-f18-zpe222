//Zack Elliott
//Sorting - in class
//Lab10

public class lab10{
  
  public static void main(String[] args){
    int[][] list = new int[3][3];
    list[0][0] = 3; 
    list[0][1] = 7;
    list[0][2] = 1;
    list[1][0] = 13;
    list[1][1] = 2;
    list[1][2] = 45;
    list[2][0] = 67;
    list[2][1] = 21;
    list[2][2] = 17;
  }
  
  public static void arraySort2DSelection(int[][] list){
    for(int i=0; i<list.length-1; i++){
      int currentMin = list[i];
      int currentMinIndex = i;
      for (int j=i+1; j<list.length; j++){
        if(currentMin>list[j]){
          currentMin=list[j];
          currentMinIndex=j;
        }
      }
      if(currentMinIndex!=i){
        list[currentMinIndex]=list[i];
        list[i] = currentMin;
      }
    }
    for(int i=0; i<list.length; i++){
      System.out.print(list[i]+" ");
    }
  }
  
  public static void arraySort2DInsertion(int[][] list){
    for (int i=1; i<list.length; i++){
      int currentElement = list[i];
      for(int k = i-1; k>=0 && list[k]>currentElement; k--){
        list[k+1] = list[k];
      }
      list[k+1] = currentElement;
    }
    for(int i=0; i<list.length; i++){
      System.out.print(list[i]+" ");
    }
  }
    
}
    