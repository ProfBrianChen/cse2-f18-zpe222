//Zack Elliott
//CSE 2 - 210
//Sept 12
//Lab03 - Allows the user to enter information and calculate the best way to split up a check

import java.util.Scanner;

public class Check{
  //main method required for every Java program
  public static void main(String[] args) {
    
  Scanner myScanner = new Scanner(System.in);
  System.out.print("Enter the original cost of the check in the form of xx.xx: "); //prompts the user for the check cost
  double checkCost = myScanner.nextDouble(); //stores user input as a double
  System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //prompts user for their tip percentage
  double tipPercent = myScanner.nextDouble(); //stores user input as a double
  tipPercent /= 100; //convert the tip percentage into a decimal value
  System.out.print("Enter the number of people who went out to dinner: "); //prompts the user for the size of their dinner party
  int numPeople = myScanner.nextInt(); //stores user input as an integer
    
  double totalCost; //total cost of the dinner
  double costPerPerson; //cost each person will have to pay
  int dollars, //whole dollar amount of the cost
  dimes, pennies; //for storing digits to the right of the decimal point for the cost in dollars
  totalCost = checkCost*(1+tipPercent); //total cost of dinner with the tip included
  costPerPerson = totalCost/numPeople; //equation to find the cost per person
   //gets the whole amount, without the decimal 
  dollars = (int)costPerPerson;
   //get the dimes amount, eg.,
  //(int)(6.73*10) % 10 -> 67 % 10 -> 7
  //where the mod operator returns the remainder
  //after the division: 583 % 100 -> 83, 27 % 5 -> 2
  dimes = (int)(costPerPerson*10) % 10;
  pennies = (int)(costPerPerson*100) % 10; //get the pennies amount
  System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //final output for the amount each person owes
  
  
  } //end of main method
} //end of class