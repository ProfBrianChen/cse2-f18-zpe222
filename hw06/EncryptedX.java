//Zack Elliott
//CSE 2
//October 21
//Hw06
//print out the encrypted X pattern

import java.util.Scanner; //importing the scanner

public class EncryptedX{
  //main method for every java program
  public static void main(String args[]){
    
    int rows=0; //variable for row number
    int columns=0; //variable for column number
    int input=0; //variable for user input
    String junk; //clears out the user input when it is not correct
    boolean correctInput = false; //set the boolean variable to false
    Scanner scanner = new Scanner(System.in); //initializing the scanner
    
    while (!correctInput){ //while the variable correct input is not true
      System.out.println("Enter an integer from 1-100: ");
      if (scanner.hasNextInt()){ //if the scanner picks up an integer from user input
        input = scanner.nextInt(); //input is equal to the integer entered by the user
        correctInput = true; //correct input becomes true
        if (input<1 || input>100){ //if the input is not between 1-10
          System.out.println("You did not enter a correct integer, try again.");
          correctInput = false; //correctInput becomes false
        }
      }
      else { //if the scanner does NOT pick up an integer from the user input
        junk = scanner.next(); //clear out the input
        System.out.println("You did not enter a correct integer, try again.");
      }
    }
    
    input = input+1; //increasing input by 1: make sure the number of rows/columns is correct
    
    for (rows=1; rows<input; rows++){ //first for loop, using "rows"
      for (columns=1; columns<input; columns++){ //nested for loop, using "columns"
        if (rows==columns || (rows+columns)==input){
          //if statement is used to print out a blank space for the X pattern
          System.out.print(" ");
        }
        else {
          //else statement, prints out * for the rest of the grid in the X pattern
          System.out.print("*");
        }
      }
      System.out.println();
    }
    
    
  }
  //end of class and main method
}
