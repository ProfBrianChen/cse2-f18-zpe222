//Zack Elliott
//CSE 2 - 210
//Sept 9
//hw02 - Arithmetic Calculations

public class Arithmetic{
  //main method required for every java program
  public static void main(String[] args) {

//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//Cost per belt
double beltCost = 33.99;

//The tax rate
double paSalesTax = 0.06;
    
double totalCostPants = numPants*pantsPrice; //total cost of pants
double totalCostShirts = numShirts*shirtPrice; // total cost of shirts
double totalCostBelts = numBelts*beltCost; //total cost of belts
double salesTaxPants = totalCostPants*paSalesTax; //sales tax on pants
double salesTaxShirts = totalCostShirts*paSalesTax; //sales tax on shirts
double salesTaxBelts = totalCostBelts*paSalesTax; //sales tax on belts
    
double totalCostNoTax = totalCostPants+totalCostShirts+totalCostBelts; //total cost without tax
double totalTax = salesTaxPants+salesTaxShirts+salesTaxBelts; //total sales tax
double totalCostTransaction = totalCostNoTax+totalTax; //total cost of transaction

//cut off extra decimals
salesTaxPants = (((int)(salesTaxPants*100))/100.0);
salesTaxShirts = (((int)(salesTaxShirts*100))/100.0);
salesTaxBelts = (((int)(salesTaxBelts*100))/100.0);
totalTax = (((int)(totalTax*100))/100.0);
totalCostTransaction = (((int)(totalCostTransaction*100))/100.0);
    
//now display all of the data
System.out.println("The total cost of the pants is: "+totalCostPants+".");
System.out.println("The total cost of the shirts is: "+totalCostShirts+".");
System.out.println("The total cost of the belts is: "+totalCostBelts+".");
System.out.println("The total sales tax on the pants is: "+salesTaxPants+".");
System.out.println("The total salex tax on the shirts is: "+salesTaxShirts+".");
System.out.println("The total sales tax on the belts is: "+salesTaxBelts+".");
System.out.println("The total cost of the transaction without tax is: "+totalCostNoTax+".");
System.out.println("The total sales tax of the transaction is: "+totalTax+".");
System.out.println("The total cost of the transaction is: "+totalCostTransaction+".");

  }//end of main method
}//end of class



