//Zack Elliott
//CSE 2 - October 3
//Lab 05 - using the scanner class

import java.util.Scanner; //importing the scanner

public class lab05{
  //main method required for every java program
  public static void main(String[] args){
  
  int courseNum; //variable for course number
  String departmentName; //variable for department name 
  int meetingsPerWeek; //variable for meetings per week
  int courseMeetingTime; // variable for course meeting time
  String instructorFirstName; //variable for instructor first name
  String instructorLastName; //variable for instructor last name
  int numStudents; //variable for number of students
  String junk; //variable for junk
  Scanner scanner = new Scanner(System.in); //initializing the scanner
  
  
  System.out.println("What is the course number: "); //asking the user for input: course number
  while (!scanner.hasNextInt()){ //while loop, check if the user entered an integer for course number
    System.out.println("You did not enter an integer, try again"); //if the user did not enter an integer, the program loops back and asks for another input
    junk = scanner.nextLine(); //if the input was incorrect, it scans it to the junk file
  }
  courseNum = scanner.nextInt(); //if the user enters an integer, the scanner accepts the input
  
  System.out.println("What is the department name: "); //same as the first block of code, this time for department name
  while (!scanner.hasNext()){
    System.out.println("You did not enter an integer, try again");
    junk = scanner.nextLine();
  }
  departmentName = scanner.next();
  
  System.out.println("How many times does the course meet per week: "); //same block of code, this time for number of meetings per week
  while (!scanner.hasNextInt()){
    System.out.println("You did not enter an integer, try again");
    junk = scanner.nextLine();
    }
  meetingsPerWeek = scanner.nextInt();
  
  System.out.println("What time of day does the course meet, in the form of XXXX military time: "); //same block of code, this time for class time
  while (!scanner.hasNextInt()){
    System.out.println("You did not enter an integer, try again");
    junk = scanner.nextLine();
    }
  courseMeetingTime = scanner.nextInt();
  
  System.out.println("What is the instructor's first name: "); //same block of code, this time for instructor's first name
  while (!scanner.hasNext()){
    System.out.println("You did not enter an integer, try again");
    junk = scanner.nextLine();
    }
  instructorFirstName = scanner.next();
    
  System.out.println("What is the instructor's last name: "); //same block of code, this time for instructor's last name
  while (!scanner.hasNext()){
    System.out.println("You did not enter an integer, try again");
    junk = scanner.nextLine();
    }
  instructorLastName = scanner.next();
  
  System.out.println("What is the number of students in the course: "); //same block of code, this time for number of students in the course
  while (!scanner.hasNextInt()){
    System.out.println("You did not enter an integer, try again");
    junk = scanner.nextLine();
    }
  numStudents = scanner.nextInt();
  
  }
}