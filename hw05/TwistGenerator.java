//Zack Elliott
//CSE 2 - hw05
//October 8th
//Twister Program: this program outputs the twister pattern with a length determined by user input

import java.util.Scanner; //importing the scanner
  
public class TwistGenerator{
  //main method for every java program
  public static void main(String args[]){
    
    int length; //variable for the length that the user will input
    int i; //variable that will be used for the for loops
    int numTwists;
    String junk; //variable needed to clear out an input line in the case of a non-integer input
    Scanner scanner = new Scanner(System.in); //defining the scanner variable
    
    System.out.println("Enter a positive integer for the length of the twist:"); //asking the user for their input length
    while (!scanner.hasNextInt()){
      //while loop, this line states "if the scanner does not have a variable"
      System.out.println("You did not enter a positive integer, try again.");
      //if the user does not enter correct input, it asks them to try again
      junk = scanner.nextLine(); //junk clears out the incorrect input
    }
    length = scanner.nextInt(); //if the input is correct, it is scanned into "length"
    
    
    for (i = 1; i <= length; ++i){ //first for loop for generating the twist pattern
      if (i%3 == 1){ //if i modulus 3 is 1, this should be the output
        System.out.print("\\");
      }
      else if (i%3 == 2){ //if i modulus 3 is 2, this should be the output
        System.out.print(" ");
      }
      else if (i%3 == 0){ //if i modulus 3 is 0, this should be the output
        System.out.print("/");
      }
    }
    
    System.out.println(""); //moves to the new line, to move to the second line of the twist pattern
    
    for (i = 1; i <= length; ++i){ //another for loop, that does the same thing, only for the second line of the twist pattern
      if (i%3 == 1){
        System.out.print(" ");
      }
      else if (i%3 == 2){
        System.out.print("X");
      }
      else if (i%3 == 0){
        System.out.print(" ");
      }
    }

    System.out.println(""); //moves to the new line, to move to the third line of the twist pattern
    
    for (i = 1; i <= length; ++i){ //another for loop, does the same thing, only for the third line of the twist pattern
      if (i%3 == 1){
        System.out.print("/");
      }
      else if (i%3 == 2){
        System.out.print(" ");
      }
      else if (i%3 == 0){
        System.out.print("\\");
      }
    }
    


  } //end of class and main method
}
