//Zack Elliott
//CSE2 - HW08
//November 12, 2018

import java.util.Arrays;
import java.util.Scanner;
import java.util.Arrays;

public class hw08{ //class
  public static void main(String[] args){ //main method
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      
    } 
    
    printArray(cards); 
    System.out.println("Shuffled");
    shuffle(cards); 
    printArray(cards); 
    while(again == 1){ 
      System.out.println("Hand");
      hand = getHand(cards, index, numCards); 
      printArray(hand);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
  } //end of main method
  
  public static void printArray(String[] list){
    for (int i=0; i<list.length; i++){ //for loop to implement numbers into the array "list"
      System.out.print(list[i]+" "); 
    }
    System.out.println();
  } //end of printArray method
    
  public static void shuffle(String[] list){
    for (int i = 0; i<52; i++){
      int index = (int)(Math.random() * list.length); //using math random to choose a random value in deck
      String temp = list[i];
      list[i] = list[index];
      list[index] = temp; //using a temporary value to swap cards in the deck
    }
  } //end of shuffle method
  
  public static String[] getHand(String[] list, int index, int numCards){
    String[] hand = new String[numCards];
    for (int i=0; i<numCards; i++){ //numCards is 5, so each hand will have 5 cards
      hand[i] = list[index];
      index--;
    }
    return hand;
  } //end of getHand method
} //end of class
