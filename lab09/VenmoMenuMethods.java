//Zack Elliott
//Venmo Activity 7
//Lab 9

import java.util.Scanner;
public class VenmoActivity7{
  public static void main(String [] args){
    Scanner input = new Scanner(System.in);
    int choice;
    double balance = 100;
    double money = 0;
    int i = 0;
    String friend;
    String friendsNames [] = new String[5];
    double moneySent [] = new double[5];
    double moneyReceived [] = new double[5];
    friendsNames[0] = "Zack";
    friendsNames[1] = "Jack";
    friendsNames[2] = "John";
    do{
      printMenu();
      choice = getInt();   
      switch (choice){
        case 1: sendMoney(balance, friendsNames, moneySent);    
                break;
        case 2: requestMoney(balance, friendsNames, moneyReceived);
                break;
        case 3: getBalance(balance);
                break;
        case 4: addFriends(friendsNames, moneySent, moneyReceived);
                break;       
        case 5: printFriendsReport(friendsNames, moneySent, moneyReceived);
                break;        
        case 6: System.out.println("Goodbye");
                break;
        default: System.out.println("you entered an invalid value -- try again");
                break;
      }
    }while(choice != 4);
  }
  
  
  //print Venmo Main Menu
  public static void printMenu(){
      System.out.println("Venmo Main Menu");
      System.out.println("1. Send Money");
      System.out.println("2. Request Money");
      System.out.println("3. Check Balance");
      System.out.println("4. Add Friends");
      System.out.println("5. Print Friend Report");
      System.out.println("6. Quit");
  }
  
  //get a number and check for an integer
  public static int getInt(){
    Scanner input = new Scanner(System.in);
     while(input.hasNextInt() == false){
        System.out.println("You entered and invalid value -- try again");
        input.nextLine();//clear buffer
     }
     return input.nextInt();
  } 
  public static void getBalance(double balance){ 
    System.out.println("You have $" + balance + " in your account" );
  }
  
  //get a number and check for a double
  public static double getDouble(){
    Scanner input = new Scanner(System.in);
    while(input.hasNextDouble() == false){
         System.out.println("You entered and invalid value -- try again");
         input.nextLine(); //clear buffer
     } 
     return input.nextDouble();
  }
  
  //driver method for sendMondy
  public static double sendMoney(double balance, String friendsNames[], double[] moneySent){
      Scanner scanner = new Scanner(System.in);
      double money = 0;
      System.out.println("Your Friends: ");
      System.out.println("1: Zack");
      System.out.println("2: Jack");
      System.out.println("3: John");
      System.out.println("Who do you want to send money to? Type in the number next to friend's name: ");
      int sendChoice = scanner.nextInt();
      if (sendChoice<1 || sendChoice>5){
        printMenu();
      }
      System.out.println("How much money do you want to send?");
      money = getDouble();//get and check user input for a double
      moneySent[sendChoice]= moneySent[sendChoice] + money;
      checkMoney(money,balance);
      balance = balance - money;      
//check valid amount no negative and if enough money is in balance to send money 
      return balance;
  }
  
  //method prints friend and money sent to friend
  public static void getFriend(double money){
        Scanner input = new Scanner(System.in);
        System.out.println("Who do you want to send your money to?");
        String friend = input.nextLine();
        System.out.println("You have successfully sent "+ money +" to "+ friend);
  }
  
  /*method that makes sure money entered is greater than 0, 
   * there are sufficient funds in balance, if sufficient funds the balance is changed
   * and returned. if funds are not sufficient the user is taken back to the main menu
   */
  public static double checkMoney(double money, double balance){
        while (money <= 0){
            System.out.println("Invalid amount entered");
            money = getDouble();
         }
         if (money <= balance){
             balance -= money;
             getFriend(money);
           }
          else{
             System.out.println("You do not have sufficient funds for this transaction.");
          }
          return balance;
  }
  
  //driver method for requestMonday
  public static double requestMoney(double balance, String[] friendsNames, double[] moneyReceived){
      Scanner scanner = new Scanner(System.in);
      double money = 0;
      System.out.println("Your Friends: ");
      System.out.println("1: Zack");
      System.out.println("2: Jack");
      System.out.println("3: John");
      System.out.println("Who do you want to request money from? Type in the number next to friend's name: ");
      int requestChoice = scanner.nextInt();
      if (requestChoice<1 || requestChoice>5){
        printMenu();
      } 
      System.out.println("How much money do you want to request?");
      money = getDouble();
      moneyReceived[requestChoice] = moneyReceived[requestChoice] + money;
      balance = checkRequestMoney(money,balance);
      System.out.println("Once confirmed, your new balance will be $" + balance);
      return balance;
               
}
  
  //gets friends information for the Request menu item and prints the amount requested from friend
  public static void getFriendRequest(double money){
     Scanner input = new Scanner(System.in);
     System.out.println("Who do you want to request money from?");
     String friend = input.nextLine();  
     System.out.println("You are requesting $" + money + " from "+ friend);
  }
  
  //method makes sure money is greater than 0 and adds requested amount to balance and returns new balance
  public static double checkRequestMoney(double money, double balance){
      while (money <= 0){
           System.out.println("Invalid amount entered");
           money = getDouble();
      }
      balance += money;
      getFriendRequest(money);
      return balance;
  }
  
  public static String addFriends(String friendsNames[], double[] moneySent, double[] moneyReceived){
    Scanner scanner = new Scanner(System.in);
    String input = " ";
    System.out.println("If you would like to add another friend, type '1'. If you do not want to add another, type '2'.");
    int anotherFriend = scanner.nextInt();
    if (anotherFriend == 1){
      System.out.println("Enter the name of the friend that you are adding: ");
      input = scanner.next();
      friendsNames[4] = input;
      moneySent[4] = 0.0;
      moneyReceived[4] = 0.0;
    }
    System.out.println("If you would like to add one more friend, type '1'. If you do not want to add another, type '2'.");
    int anotherFriend2 = scanner.nextInt();
    if (anotherFriend2 == 1){
      System.out.println("Enter the name of the friend that you are adding: ");
      input = scanner.next();
      friendsNames[5] = input;
      moneySent[5] = 0.0;
      moneyReceived[5] = 0.0;
    }
    return input;
  }
  
  public static void printFriendsReport(String friendsNames[], double[] moneySent, double[] moneyReceived){
    for (int i=0; i<5; i++){
      System.out.println(friendsNames[i] + " $" + moneySent[i] + " $" + moneyReceived[i]);
    }
      
  }
 
  
}   