/////
//HW 01 Welcome Message
//Zack Elliott
//Sept 3
//CSE 2 - 210
//
public class WelcomeClass{
  public static void main(String args[]){
    //prints a Welcome message to terminal window
    System.out.println("-----------");
    System.out.println("| Welcome |");
    System.out.println("-----------");
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-Z--P--E--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v  ");
    System.out.println("Hello, my name is Zack and I play tennis at Lehigh.");
    
    
  }
}
