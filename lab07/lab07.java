//Zack Elliott
//CSE 2
//Lab07 - Methods
//Using strings to create random sentences

import java.util.Scanner; //importing the scanner
import java.util.Random; //importing the Random generator

public class lab07{
  
  static String paragraphSubject = " ";
  
  public static String Adjective(){ //first method, generates random adjectives
    Random randomGenerator = new Random(); //defining the random generator
    int randomInt = randomGenerator.nextInt(10); //generating a random integer from 0-9
    String adjective = " ";
    switch (randomInt){ //switch statement to choose 1 of the 10 adjectives
      case 0:
        adjective = "fun";
        break;
      case 1:
        adjective = "cool";
        break;
      case 2:
        adjective = "good";
        break;
      case 3:
        adjective = "bad";
        break;
      case 4:
        adjective = "warm";
        break;
      case 5:
        adjective = "cold";
        break;
      case 6:
        adjective = "nice";
        break;
      case 7:
        adjective = "mean";
        break;
      case 8:
        adjective = "big";
        break;
      case 9:
        adjective = "small";
        break;
    } //end of switch statement
    return adjective;
  } //end of method
    
  public static String Noun(){ //method to generate a noun
    Random randomGenerator = new Random(); //defining the random generator within a method
    int randomInt = randomGenerator.nextInt(10); //generates a random integer from 0-9
    String noun = " ";
    switch (randomInt){ //switch statement to choose 1 of the 10 nouns
      case 0:
        noun = "boy";
        break;
      case 1:
        noun = "girl";
        break;
      case 2:
        noun = "man";
        break;
      case 3:
        noun = "woman";
        break;
      case 4:
        noun = "animal";
        break;
      case 5:
        noun = "child";
        break;
      case 6:
        noun = "tree";
        break;
      case 7:
        noun = "baby";
        break;
      case 8:
        noun = "plant";
        break;
      case 9:
        noun = "chair";
        break;
    } //end of switch statement
    return noun;
  } //end of method
  
  public static String Verb(){ //method to generate verbs
    Random randomGenerator = new Random(); //defining the random generator within a method
    int randomInt = randomGenerator.nextInt(10); //generates random integer from 0-9
    String verb = " ";
    switch (randomInt){ //switch statement to choose a verb
      case 0:
        verb = "ran";
        break;
      case 1:
        verb = "moved";
        break;
      case 2:
        verb = "swam";
        break;
      case 3:
        verb = "ate";
        break;
      case 4:
        verb = "drank";
        break;
      case 5:
        verb = "worked";
        break;
      case 6:
        verb = "sat";
        break;
      case 7:
        verb = "stood";
        break;
      case 8:
        verb = "swallowed";
        break;
      case 9:
        verb = "talked";
        break;
    } //end of switch
    return verb;
  } //end of method
  
  public static String Noun2(){ //method to generate a second noun
    Random randomGenerator = new Random(); //defining the random generator within a method
    int randomInt = randomGenerator.nextInt(10); //generates random integer from 0-9
    String noun2 = " ";
    switch (randomInt){ //switch statement to choose a second noun
      case 0:
        noun2 = "toy";
        break;
      case 1:
        noun2 = "table";
        break;
      case 2:
        noun2 = "pen";
        break;
      case 3:
        noun2 = "paper";
        break;
      case 4:
        noun2 = "water";
        break;
      case 5:
        noun2 = "food";
        break;
      case 6:
        noun2 = "shoes";
        break;
      case 7:
        noun2 = "shirt";
        break;
      case 8:
        noun2 = "pants";
        break;
      case 9:
        noun2 = "hat";
        break;
    } //end of switch
    return noun2;
  } //end of method
      
  public static String Sentence(){ //sentence method
    String noun = Noun(); //noun is the variable that will be returned from this method
    String mySentence = "The "+Adjective()+" "+ noun +" "+Verb()+" the "+Adjective()+" "+Noun2()+".";
    System.out.println(mySentence);
    return noun;
  }
  
  public static void ActionSentence(String noun){ //action sentence method
    System.out.println("This "+Adjective()+" "+noun+" "+Verb()+" the "+Adjective()+" "+Noun2()+".");
  } //generates action sentences with the same subject as the first sentence
  
  public static void ConclusionSentence(String noun){ //conclusion sentence method
    Scanner scanner = new Scanner(System.in);
    System.out.println("That "+noun+" "+Verb()+" their "+Noun2()+".");
    String conclusionSentence = scanner.nextLine();
  }
  
  public static void Paragraph(){ //paragraph method
    String noun = Sentence(); //calling sentence method
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); //generates a random number of action sentences for the paragraph
    int i = 0;
    for (i = 0; i<=randomInt; i++){
    ActionSentence(noun);
    }
    ConclusionSentence(noun);
  }
      
  
  public static void main(String args[]){ //main method
    Scanner scanner = new Scanner(System.in);
    int createSentence = 1;
    while (createSentence == 1){ //loop that continues to ask user if they want to create more sentences
    Sentence();
    System.out.print("Enter the number 1 if you would like to create another sentence:");
    createSentence = scanner.nextInt();
    }
    Paragraph(); //runs the paragraph method after the initial sentence loop
  }
    
        
        
  
  
} //end of class