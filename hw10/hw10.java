///Zack Elliott
//CSE2 - HW10
//TIC-TAC-TOE

import java.util.Scanner;
import java.util.Arrays;

public class hw10{
  public static void main(String[] args){
    Scanner scanner = new Scanner(System.in); //initializing scanner
    String[][] gameBoard = new String[3][3]; //two dimensional array called gameBoard
    int playX = 0;
    int playO = 0;
    //filling in the array with numbers
    gameBoard[0][0] = "1"; 
    gameBoard[0][1] = "2";
    gameBoard[0][2] = "3";
    gameBoard[1][0] = "4";
    gameBoard[1][1] = "5";
    gameBoard[1][2] = "6";
    gameBoard[2][0] = "7";
    gameBoard[2][1] = "8";
    gameBoard[2][2] = "9";
    boolean winner = false;
    boolean occupied = false;
    System.out.println("Here is the game board: ");
    printBoard(gameBoard); //calling the other methods in order to run the game
    playerX(gameBoard);
    printBoard(gameBoard);
    playerO(gameBoard); //alternate between turns for Player X and Player O
    printBoard(gameBoard);
    playerX(gameBoard);
    printBoard(gameBoard);
    playerO(gameBoard);
    printBoard(gameBoard);
    playerX(gameBoard);
    printBoard(gameBoard);
    playerO(gameBoard);
    printBoard(gameBoard);
    playerX(gameBoard);
    printBoard(gameBoard);
    playerO(gameBoard);
    printBoard(gameBoard);
    playerX(gameBoard);
    printBoard(gameBoard);
    //end of game
    
        
    
  } //end of main method
  
  public static void playerX(String[][] gameBoard){ //method to call the actions of player X
    int playO = 0;
    boolean winnerX = false;
    boolean occupied = false;
    Scanner scanner = new Scanner(System.in);
    System.out.println("Player X: Enter the number of the space that you want to play your 'X'.");
    //checking the user input
    if (!scanner.hasNextInt()){
      System.out.println("You did not enter an integer, try again:");
    }
    int playX = scanner.nextInt();
    if (playX<1 || playX>9){
      System.out.println("Your number is not between 1 and 9, try again:");
    }
    
    //replacing the numbers in the game board with an X
    if (playX == 1){
      gameBoard[0][0] = "X";
    }
    else if (playX == 2){
      gameBoard[0][1] = "X";
    }
    else if (playX == 3){
      gameBoard[0][2] = "X";
    }
    else if (playX == 4){
      gameBoard[1][0] = "X";
    }
    else if (playX == 5){
      gameBoard[1][1] = "X";
    }
    else if (playX == 6){
      gameBoard[1][2] = "X";
    }
    else if (playX == 7){
      gameBoard[2][0] = "X";
    }
    else if (playX == 8){
      gameBoard[2][1] = "X";
    }
    else if (playX == 9){
      gameBoard[2][2] = "X";
    }
    
    winnerX = winnerX(gameBoard);
      if (winnerX==true){
        System.out.println("PLayer X has won!");
      }

  } //end of playerX method
  
  public static void playerO(String[][] gameBoard){ //method for the actions of player O
    int playX = 0;
    boolean winnerO = false;
    boolean occupied = false;
    Scanner scanner = new Scanner(System.in);
    System.out.println("Player O: Enter the number of the space that you want to play your 'O'.");
    if (!scanner.hasNextInt()){
      System.out.println("You did not enter an integer, try again:");
    }
    int playO = scanner.nextInt();
    if (playO<1 || playO>9){
      System.out.println("Your number is not between 1 and 9, try again:");
    }

    
    //replacing the numbers on the game board with an O
    if (playO == 1){
      gameBoard[0][0] = "O";
    }
    else if (playO == 2){
      gameBoard[0][1] = "O";
    }
    else if (playO == 3){
      gameBoard[0][2] = "O";
    }
    else if (playO == 4){
      gameBoard[1][0] = "O";
    }
    else if (playO == 5){
      gameBoard[1][1] = "O";
    }
    else if (playO == 6){
      gameBoard[1][2] = "O";
    }
    else if (playO == 7){
      gameBoard[2][0] = "O";
    }
    else if (playO == 8){
      gameBoard[2][1] = "O";
    }
    else if (playO == 9){
      gameBoard[2][2] = "O";
    }
    
    winnerO = winnerO(gameBoard);
      if (winnerO==true){
        System.out.println("Player O has won!");
      }
   
  } //end of playerO method
    
  public static void printBoard(String[][] gameBoard){ //method to print out the game board
    //need two for loops to print out a multidimensional array
    for (int row=0; row<gameBoard.length; row++){
      for (int col=0; col<gameBoard[row].length; col++){
        System.out.print(gameBoard[row][col]+ "  ");
      }
      System.out.println();
    }
  } //end of printBoard method
  
  public static boolean checkSpot(String[][] gameBoard){
    boolean occupied = false;
    for (int i = 0; i<gameBoard.length; i++){
      for (int j = 0; j<gameBoard[0].length; j++){
        if ((gameBoard[i][j] == "O") || (gameBoard[i][j] == "X")){
          occupied = true;
        }
      }
    }
    return occupied;
  }
  
  public static boolean winnerX(String[][] gameBoard){
    //this is the method to check on whether on not player X has gotten three in a row
    boolean winnerX = false;
    if (((gameBoard[0][0].equals(gameBoard[0][1]))&&(gameBoard[0][1].equals(gameBoard[0][2]))) ||
       ((gameBoard[1][0].equals(gameBoard[1][1]))&&(gameBoard[1][1].equals(gameBoard[1][2]))) ||
       ((gameBoard[2][0].equals(gameBoard[2][1]))&&(gameBoard[2][1].equals(gameBoard[2][2]))) ||
       ((gameBoard[0][0].equals(gameBoard[1][0]))&&(gameBoard[1][0].equals(gameBoard[2][0]))) ||
       ((gameBoard[0][1].equals(gameBoard[1][1]))&&(gameBoard[1][1].equals(gameBoard[2][1]))) ||
       ((gameBoard[0][2].equals(gameBoard[1][2]))&&(gameBoard[1][2].equals(gameBoard[2][2]))) ||
       ((gameBoard[0][0].equals(gameBoard[1][1]))&&(gameBoard[1][1].equals(gameBoard[2][2]))) ||
       ((gameBoard[0][2].equals(gameBoard[1][1]))&&(gameBoard[1][1].equals(gameBoard[2][0])))){
       winnerX = true;
    }
    return winnerX; //returning boolean type
        
  } //end of winnerX method
  
  public static boolean winnerO(String[][] gameBoard){
    //this is the method to check on whether on not player O has gotten three in a row
    boolean winnerO = false;
    if (((gameBoard[0][0].equals(gameBoard[0][1]))&&(gameBoard[0][1].equals(gameBoard[0][2]))) ||
       ((gameBoard[1][0].equals(gameBoard[1][1]))&&(gameBoard[1][1].equals(gameBoard[1][2]))) ||
       ((gameBoard[2][0].equals(gameBoard[2][1]))&&(gameBoard[2][1].equals(gameBoard[2][2]))) ||
       ((gameBoard[0][0].equals(gameBoard[1][0]))&&(gameBoard[1][0].equals(gameBoard[2][0]))) ||
       ((gameBoard[0][1].equals(gameBoard[1][1]))&&(gameBoard[1][1].equals(gameBoard[2][1]))) ||
       ((gameBoard[0][2].equals(gameBoard[1][2]))&&(gameBoard[1][2].equals(gameBoard[2][2]))) ||
       ((gameBoard[0][0].equals(gameBoard[1][1]))&&(gameBoard[1][1].equals(gameBoard[2][2]))) ||
       ((gameBoard[0][2].equals(gameBoard[1][1]))&&(gameBoard[1][1].equals(gameBoard[2][0])))){
       winnerO = true;
    }
    return winnerO; //returning boolean type
        
  } //end of winnerO method
      
      
      
      
} //end of class