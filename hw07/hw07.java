//Zack Elliott
//CSE2 - HW07
//10-28-18
//Word Tools

import java.util.Scanner; //importing the scanner

public class hw07{
  
  public static String sampleText(){ //sample text method
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter a string of your choice:"); //asking the user to input a string
    String userString = scanner.nextLine();
    System.out.println(userString);
    return userString;
  } //end of method
  
  public static String printMenu(){ //print menu method
    Scanner scanner = new Scanner(System.in);
    String menuChoice = " ";
    System.out.println("MENU:"); //printing out the menu to the user
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("Enter your menu option: ");
    menuChoice = scanner.next();
    return menuChoice;
} //end of method
  
  public static int getNumOfNonWSCharacters(String userString){ //number of characters method
    userString = userString.replace(" ","");
    int stringLength;
    stringLength = userString.length();
    return stringLength;
  } //end of method
   
  public static int getNumOfWords(String userString){ //number of words method
    Scanner scanner = new Scanner(System.in);
    int wordCount = 1;
    int i = 0;
    for (i=0; i<userString.length(); i++){ //for loop to count the number of words
      if (userString.charAt(i)==' ' && userString.charAt(i+1)!=' '){ //avoids couting a double space as 2 different words
        wordCount++;
      }
    }
    System.out.println("The number of words is: ");
    return wordCount;
  } //end of method
  
  public static int findText(String userString){ //find text method
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter the word you are trying to find: ");
    String foundText = scanner.next();
    int counter = 0;
    int indexCount = userString.indexOf(foundText); //using the index function to find the position of the occurence of the desired word
    while (indexCount >= 0){
      indexCount = userString.indexOf(userString, indexCount + 1);
      counter = counter + 1;
    }
    return counter;
    
  } //end of method
  
  public static String replaceExclamation(String userString){ //replace exclamation method
    userString = userString.replace("!",".");
    return userString;
  } //end of method
  
  public static String shortenSpace(String userString){ //shorten space methods
    userString = userString.replace("  ", " ");
    userString = userString.replace("   ", " ");
    userString = userString.replace("    ", " ");
    userString = userString.replace("     ", " ");
    return userString;
  } //end of method
  
  public static void main(String args[]){ //main method
    String menuChoice = printMenu();
    //based on the user menu choice, the main method will run the corresponding method
    if (menuChoice.equals("c")){
      System.out.println(getNumOfNonWSCharacters(sampleText()));
    }
    else if (menuChoice.equals("w")){
      System.out.println(getNumOfWords(sampleText()));
    }
    else if (menuChoice.equals("f")){
      System.out.println(findText(sampleText()));
      System.out.println("The number of occurences is printed above: ");
    }
    else if (menuChoice.equals("r")){
      System.out.println(replaceExclamation(sampleText()));
    }
    else if (menuChoice.equals("s")){
      System.out.println(shortenSpace(sampleText()));
    }
    else if (menuChoice.equals("q")){
     System.exit(0); 
    }
    else {
      System.out.println("You did not enter a valid menu option.");
      printMenu();
    }
  } //end of main method
  
}
    