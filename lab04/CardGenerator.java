//Zack Elliott
//CSE 2 - Lab 04
//This program generates a random card from a 52 card deck

public class CardGenerator{
  //main method required for every java program
  public static void main(String[] args){
    
  String suitName; //variable for the type of card suit
  String cardIdentity = ""; //variable for the specific card type within each suit
  int randomNumber; //variable that will be needed to generate random numbers
  
  randomNumber = (int)(Math.random()* 51 )+1; //generates a random integer from 1-52
    
  if ((randomNumber>=1)&&(randomNumber<=13)){ //beginning of the if statement to declare the suit of the card
    suitName = "Diamonds";                    //any random number from 1-13 will represent a Diamond card
  }
  else if ((randomNumber>=14)&&(randomNumber<=26)){ //any random number from 14-26 will represent a Clubs card
    suitName = "Clubs";
  }
  else if ((randomNumber>=27)&&(randomNumber<=39)){ //any random number from 27-39 will represent a Hearts card
    suitName = "Hearts";
  }
  else {  //any random number from 40-52 will represent a Diamond card
    suitName = "Spades";
  }
 
  switch (randomNumber%13){ //beginning of the switch statements to assign a numerical value to a card identity
    case 0:
      cardIdentity = "King"; //if the modolus returns a value of 0, it represents a King card
      break;
      
    case 1:
      cardIdentity = "Ace"; //if the modolus returns a value of 1, it represents an Ace card
      break;
      
    case 2:
      cardIdentity = "2"; //if the modolus returns a value of 2, it represents a 2 card
      break;
      
    case 3:
      cardIdentity = "3"; //if the modolus returns a value of 3, it represents a 3 card
      break;
      
    case 4:
      cardIdentity = "4"; //if the modolus returns a value of 4, it represents a 4 card
      break;
      
    case 5:
      cardIdentity = "5"; //if the modolus returns a value of 5, it represents a 5 card
      break;
      
    case 6:
      cardIdentity = "6"; //if the modolus returns a value of 6, it represents a 6 card
      break;
      
    case 7:
      cardIdentity = "7"; //if the modolus returns a value of 7, it represents a 7 card
      break;
      
    case 8:
      cardIdentity = "8"; //if the modolus returns a value of 8, it represents a 8 card
      break;
      
    case 9:
      cardIdentity = "9"; //if the modolus returns a value of 9, it represents a 9 card
      break;
      
    case 10:
      cardIdentity = "10"; //if the modolus returns a value of 10, it represents a 10 card
      break;
      
    case 11:
      cardIdentity = "Jack"; //if the modolus returns a value of 11, it represents a Jack card
      break;
      
    case 12:
      cardIdentity = "Queen"; //if the modolus returns a value of 12, it represents a Queen card
      break;
    }
    
    System.out.println("You picked the " + cardIdentity + " of " + suitName); //final output line that tells the user what random card they have selcted from the 52 deck
    
    
  } //end of main method
   
} //end of class