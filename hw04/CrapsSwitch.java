//Zack Elliott
//CSE 2 - HW 04
//This program carries out the classic casino game, Craps, using only switch statements

import java.util.Scanner; //initializes the scanner

public class CrapsSwitch{
  //main method required for every java program
  public static void main(String[] args){
    
  Scanner myScanner = new Scanner(System.in); //defining the Scanner
  int dice1; //variable for the first dice in the game
  int dice2; //variable for the second dice in the game
  
  //need to ask the user if they want to randomly cast their dice or set the values themselves   
  System.out.println("Would you like to randomly cast dice or set the two dice? Enter 1 for random cast or enter 2 for setting dice: "); 
  int userInput = myScanner.nextInt(); //this Scanner will take the user's decision for their dice
    
  //using nested switch statements to display all of the possible outcomes of Craps
  switch (userInput){ //beginning of the main switch statement, which involves whether or not the user wants to randomly cast or set dice
    case 1: //if the user input is 1, they want to randomly cast their dice
      dice1 = (int)(Math.random()* 6 )+1; //randomly assigns an integer from 1-6 for dice 1
      dice2 = (int)(Math.random()* 6 )+1; //randomly assigns an integer from 1-6 for dice 2
      switch (dice1){ //starts the switch statement for the second dice value based on the value of the first dice
        case 1: //if Dice 1 is 1, these are the following possibilities for Dice 2
          switch (dice2){ //beginning of the switch statement for what dice 2 can be if dice 1 is 1
            case 1:
              System.out.println("Snake Eyes"); //outputs the name of 1,1
              break;
            case 2:
              System.out.println("Ace Duece"); //outputs the name of 1,2
              break;
            case 3:
              System.out.println("Easy Four"); //outputs the name of 1,3
              break;
            case 4:
              System.out.println("Fever Five"); //outputs the name of 1,4
              break;
            case 5:
              System.out.println("Easy Six"); //outputs the name of 1,5
              break;
            case 6:
              System.out.println("Seven Out"); //outputs the name of 1,6
              break;
            default: System.out.println("Invalid");
          }
          break;
        case 2: //if Dice 1 is 2, then these are the possibilities for dice 2
          switch (dice2){
            case 1:
              System.out.println("Ace Duece");
              break;
            case 2:
              System.out.println("Hard Four");
              break;
            case 3:
              System.out.println("Fever Five");
              break;
            case 4:
              System.out.println("Easy Six");
              break;
            case 5:
              System.out.println("Seven Out");
              break;
            case 6:
              System.out.println("Easy Eight");
              break;
            default: System.out.println("Invalid");
          }
          break;
        case 3: //if Dice 1 is 3, then these are the possibilities for dice 2
          switch (dice2){
            case 1:
              System.out.println("Easy Four");
              break;
            case 2:
              System.out.println("Fever Five");
              break;
            case 3:
              System.out.println("Hard Six");
              break;
            case 4:
              System.out.println("Seven Out");
              break;
            case 5:
              System.out.println("Easy Eight");
              break;
            case 6:
              System.out.println("Nine");
              break;
            default: System.out.println("Invalid");
          }
          break;
        case 4: //if Dice 1 is 4, then these are the possibilities for dice 2
          switch (dice2){
            case 1:
              System.out.println("Fever Five");
              break;
            case 2:
              System.out.println("Easy Six");
              break;
            case 3:
              System.out.println("Seven Out");
              break;
            case 4:
              System.out.println("Hard Eight");
              break;
            case 5:
              System.out.println("Nine");
              break;
            case 6:
              System.out.println("Easy Ten");
              break;
            default: System.out.println("Invalid");
          }
          break;
        case 5: //if Dice 1 is 5, then these are the possibilities for dice 2
          switch (dice2){
            case 1:
              System.out.println("Easy Six");
              break;
            case 2:
              System.out.println("Seven Out");
              break;
            case 3:
              System.out.println("Easy Eight");
              break;
            case 4:
              System.out.println("Nine");
              break;
            case 5:
              System.out.println("Hard Ten");
              break;
            case 6:
              System.out.println("Yo-leven");
              break;
            default: System.out.println("Invalid");
          }
          break;
        case 6: //if Dice 1 is 6, then these are the possibilities for dice 2
          switch (dice2){
            case 1:
              System.out.println("Seven Out");
              break;
            case 2:
              System.out.println("Easy Eight");
              break;
            case 3:
              System.out.println("Nine");
              break;
            case 4:
              System.out.println("Easy Ten");
              break;
            case 5:
              System.out.println("Yo-leven");
              break;
            case 6:
              System.out.println("Boxcars");
              break;
            default: System.out.println("Invalid");
          }
          break;
        default: System.out.println("Invalid");
      }
      break; 
  case 2: //if the user input is 2, then the user will set the value for each of the dice
    System.out.println("Enter your desired value of the first dice: "); //asking the user for their value of dice 1
    dice1 = myScanner.nextInt(); //scanner takes the value of dice 1
    System.out.println("Enter your desired value of the second dice: "); //asking the user for their vallue of dice 2
    dice2 = myScanner.nextInt(); //scanner takes the value of dice 2
      switch (dice1){ //starts the switch statement for the possibilites of dice 1
        case 1: //if dice 1 is set as 1, then these are the following possible values for dice 2
          switch (dice2){ //beginning of the switch statement for the possibilites of dice 2
            case 1:
              System.out.println("Snake Eyes");
              break;
            case 2:
              System.out.println("Ace Duece");
              break;
            case 3:
              System.out.println("Easy Four");
              break;
            case 4:
              System.out.println("Fever Five");
              break;
            case 5:
              System.out.println("Easy Six");
              break;
            case 6:
              System.out.println("Seven Out");
              break;
            default: System.out.println("Invalid");
          }
          break;
        case 2: //if dice 1 is set as 2, then these are the following possible values for dice 2
          switch (dice2){
            case 1:
              System.out.println("Ace Duece");
              break;
            case 2:
              System.out.println("Hard Four");
              break;
            case 3:
              System.out.println("Fever Five");
              break;
            case 4:
              System.out.println("Easy Six");
              break;
            case 5:
              System.out.println("Seven Out");
              break;
            case 6:
              System.out.println("Easy Eight");
              break;
            default: System.out.println("Invalid");
          }
          break;
        case 3: //if dice 1 is set as 3, then these are the following possible values for dice 2
          switch (dice2){
            case 1:
              System.out.println("Easy Four");
              break;
            case 2:
              System.out.println("Fever Five");
              break;
            case 3:
              System.out.println("Hard Six");
              break;
            case 4:
              System.out.println("Seven Out");
              break;
            case 5:
              System.out.println("Easy Eight");
              break;
            case 6:
              System.out.println("Nine");
              break;
            default: System.out.println("Invalid");
          }
          break;
        case 4: //if dice 1 is set as 4, then these are the following possible values for dice 2
          switch (dice2){
            case 1:
              System.out.println("Fever Five");
              break;
            case 2:
              System.out.println("Easy Six");
              break;
            case 3:
              System.out.println("Seven Out");
              break;
            case 4:
              System.out.println("Hard Eight");
              break;
            case 5:
              System.out.println("Nine");
              break;
            case 6:
              System.out.println("Easy Ten");
              break;
            default: System.out.println("Invalid");
          }
          break;
        case 5: //if dice 1 is set as 5, then these are the following possible values for dice 2
          switch (dice2){
            case 1:
              System.out.println("Easy Six");
              break;
            case 2:
              System.out.println("Seven Out");
              break;
            case 3:
              System.out.println("Easy Eight");
              break;
            case 4:
              System.out.println("Nine");
              break;
            case 5:
              System.out.println("Hard Ten");
              break;
            case 6:
              System.out.println("Yo-leven");
              break;
            default: System.out.println("Invalid");
          }
          break;
        case 6: //if dice 1 is set as 6, then these are the following possible values for dice 2
          switch (dice2){
            case 1:
              System.out.println("Seven Out");
              break;
            case 2:
              System.out.println("Easy Eight");
              break;
            case 3:
              System.out.println("Nine");
              break;
            case 4:
              System.out.println("Easy Ten");
              break;
            case 5:
              System.out.println("Yo-leven");
              break;
            case 6:
              System.out.println("Boxcars");
              break;
            default: System.out.println("Invalid");
          }
          break; //end of switch statement
        default: System.out.println("Invalid");
    }
      break; //end of switch statement
    default: System.out.println("Invalid");
  }
  
    
  } //end of class
} //end of main method