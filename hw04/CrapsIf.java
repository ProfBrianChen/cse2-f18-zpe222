//Zack Elliott
//CSE 2 - HW 04
//This program carries out the classic casino game, Craps, using only if statements

import java.util.Scanner; //initializes the scanner

public class CrapsIf{
  //main method required for every java program
  public static void main(String[] args){
    
  Scanner myScanner = new Scanner(System.in); //initializing the myScanner variable
  int randomDice1; //variable for dice 1 in a random cast of dice
  int randomDice2; //variable for dice 2 in a random cast of dice
  int setDice1; //variable for the user setting dice 1
  int setDice2; //variable for the user setting dice 2
    
  //need to ask the user if they want to randomly cast the dice or if they want to set the values of the two dice:
  System.out.println("Would you like to randomly cast dice or set the two dice? Enter 1 for random cast or enter 2 for setting dice: ");
  int userInput = myScanner.nextInt(); //the scanner will take the user input from the line above
  
  //using nested if statements to portray all possible combinations of dice 1 and dice 2
  if (userInput == 1){ //beginning of the if statement for userInput
    randomDice1 = (int)(Math.random()* 6 )+1; //randomDice1 is randomly assigned to an integer from 1-6
    randomDice2 = (int)(Math.random()* 6 )+1; //randomDice2 is randomly assigned to an integer from 1-6
      if ((randomDice1 == 1)&&(randomDice2 == 1)){
        System.out.println("Snake Eyes"); //outputs the name of 1,1
      }
      else if ((randomDice1 == 1)&&(randomDice2 == 2)){ //outputs the name of 1,2
        System.out.println("Ace Duece");
      }
      else if ((randomDice1 == 1)&&(randomDice2 == 3)){ //outputs the name of 1,3
        System.out.println("Easy Four");
      }
      else if ((randomDice1 == 1)&&(randomDice2 == 4)){ //outputs the name of 1,4
        System.out.println("Fever Five");
      }
      else if ((randomDice1 == 1)&&(randomDice2 == 5)){ //clearly, there is a pattern where each print statement prints the name of a certain dice combination
        System.out.println("Easy Six");
      }
      else if ((randomDice1 == 1)&&(randomDice2 == 6)){
        System.out.println("Seven Out");
      }
      else if ((randomDice1 == 2)&&(randomDice2 == 2)){
        System.out.println("Hard Four");
      }
      else if ((randomDice1 == 2)&&(randomDice2 == 3)){
        System.out.println("Fever Five");
      }
      else if ((randomDice1 == 2)&&(randomDice2 == 4)){
        System.out.println("Easy Six");
      }  
      else if ((randomDice1 == 2)&&(randomDice2 == 5)){
        System.out.println("Seven Out");
      }
      else if ((randomDice1 == 2)&&(randomDice2 == 6)){
        System.out.println("Easy Eight");
      }
      else if ((randomDice1 == 3)&&(randomDice2 == 3)){
        System.out.println("Hard Six");
      }
      else if ((randomDice1 == 3)&&(randomDice2 == 4)){
        System.out.println("Seven Out");
      }
      else if ((randomDice1 == 3)&&(randomDice2 == 5)){
        System.out.println("Easy Eight");
      }
      else if ((randomDice1 == 3)&&(randomDice2 == 6)){
        System.out.println("Nine");
      }  
      else if ((randomDice1 == 4)&&(randomDice2 == 4)){
        System.out.println("Hard Eight");
      }
      else if ((randomDice1 == 4)&&(randomDice2 == 5)){
        System.out.println("Nine");
      }
      else if ((randomDice1 == 4)&&(randomDice2 == 6)){
        System.out.println("Easy Ten");
      }
      else if ((randomDice1 == 5)&&(randomDice2 == 5)){
        System.out.println("Hard Ten");
      }
      else if ((randomDice1 == 5)&&(randomDice2 == 6)){
        System.out.println("Yo-leven");
      }
      else if ((randomDice1 == 6)&&(randomDice2 == 6)){
        System.out.println("Boxcars");
      }  
      else if ((randomDice1 == 1)&&(randomDice2 == 1)){
        System.out.println("Snake Eyes");
      }
      else if ((randomDice1 == 2)&&(randomDice2 == 1)){
        System.out.println("Ace Duece");
      }
      else if ((randomDice1 == 3)&&(randomDice2 == 1)){
        System.out.println("Easy Four");
      }
      else if ((randomDice1 == 4)&&(randomDice2 == 1)){
        System.out.println("Fever Five");
      }
      else if ((randomDice1 == 5)&&(randomDice2 == 1)){
        System.out.println("Easy Six");
      }
      else if ((randomDice1 == 6)&&(randomDice2 == 1)){
        System.out.println("Seven Out");
      }
      else if ((randomDice1 == 2)&&(randomDice2 == 2)){
        System.out.println("Hard Four");
      }
      else if ((randomDice1 == 3)&&(randomDice2 == 2)){
        System.out.println("Fever Five");
      }
      else if ((randomDice1 == 4)&&(randomDice2 == 2)){
        System.out.println("Easy Six");
      }  
      else if ((randomDice1 == 5)&&(randomDice2 == 2)){
        System.out.println("Seven Out");
      }
      else if ((randomDice1 == 6)&&(randomDice2 == 2)){
        System.out.println("Easy Eight");
      }
      else if ((randomDice1 == 3)&&(randomDice2 == 3)){
        System.out.println("Hard Six");
      }
      else if ((randomDice1 == 4)&&(randomDice2 == 3)){
        System.out.println("Seven Out");
      }
      else if ((randomDice1 == 5)&&(randomDice2 == 3)){
        System.out.println("Easy Eight");
      }
      else if ((randomDice1 == 6)&&(randomDice2 == 3)){
        System.out.println("Nine");
      }  
      else if ((randomDice1 == 4)&&(randomDice2 == 4)){
        System.out.println("Hard Eight");
      }
      else if ((randomDice1 == 5)&&(randomDice2 == 4)){
        System.out.println("Nine");
      }
      else if ((randomDice1 == 6)&&(randomDice2 == 4)){
        System.out.println("Easy Ten");
      }
      else if ((randomDice1 == 5)&&(randomDice2 == 5)){
        System.out.println("Hard Ten");
      }
      else if ((randomDice1 == 6)&&(randomDice2 == 5)){
        System.out.println("Yo-leven");
      }
      else if ((randomDice1 == 6)&&(randomDice2 == 6)){
        System.out.println("Boxcars");
      }
      else {
        System.out.println("Invalid");
      }
  } //end of the first if statement
  else if (userInput == 2){ //beginning of the if statement for when the user wants to input their own dice values
    System.out.println("Enter your desired value of the first dice: "); //asks the user for their value for dice 1
    setDice1 = myScanner.nextInt();
    System.out.println("Enter your desired value of the second dice: "); //asks the user for their value for dice 2
    setDice2 = myScanner.nextInt();
      if ((setDice1 == 1)&&(setDice2 == 1)){ //once again, using nested if statements to address all possible dice combinations
        System.out.println("Snake Eyes"); //printing out the dice outcome names
      }
      else if ((setDice1 == 1)&&(setDice2 == 2)){
        System.out.println("Ace Duece");
      }
      else if ((setDice1 == 1)&&(setDice2 == 3)){
        System.out.println("Easy Four");
      }
      else if ((setDice1 == 1)&&(setDice2 == 4)){
        System.out.println("Fever Five");
      }
      else if ((setDice1 == 1)&&(setDice2 == 5)){
        System.out.println("Easy Six");
      }
      else if ((setDice1 == 1)&&(setDice2 == 6)){
        System.out.println("Seven Out");
      }
      else if ((setDice1 == 2)&&(setDice2 == 2)){
        System.out.println("Hard Four");
      }
      else if ((setDice1 == 2)&&(setDice2 == 3)){
        System.out.println("Fever Five");
      }
      else if ((setDice1 == 2)&&(setDice2 == 4)){
        System.out.println("Easy Six");
      }  
      else if ((setDice1 == 2)&&(setDice2 == 5)){
        System.out.println("Seven Out");
      }
      else if ((setDice1 == 2)&&(setDice2 == 6)){
        System.out.println("Easy Eight");
      }
      else if ((setDice1 == 3)&&(setDice2 == 3)){
        System.out.println("Hard Six");
      }
      else if ((setDice1 == 3)&&(setDice2 == 4)){
        System.out.println("Seven Out");
      }
      else if ((setDice1 == 3)&&(setDice2 == 5)){
        System.out.println("Easy Eight");
      }
      else if ((setDice1 == 3)&&(setDice2 == 6)){
        System.out.println("Nine");
      }  
      else if ((setDice1 == 4)&&(setDice2 == 4)){
        System.out.println("Hard Eight");
      }
      else if ((setDice1 == 4)&&(setDice2 == 5)){
        System.out.println("Nine");
      }
      else if ((setDice1 == 4)&&(setDice2 == 6)){
        System.out.println("Easy Ten");
      }
      else if ((setDice1 == 5)&&(setDice2 == 5)){
        System.out.println("Hard Ten");
      }
      else if ((setDice1 == 5)&&(setDice2 == 6)){
        System.out.println("Yo-leven");
      }
      else if ((setDice1 == 6)&&(setDice2 == 6)){
        System.out.println("Boxcars");
      }  
      else if ((setDice1 == 1)&&(setDice2 == 1)){
        System.out.println("Snake Eyes");
      }
      else if ((setDice1 == 2)&&(setDice2 == 1)){
        System.out.println("Ace Duece");
      }
      else if ((setDice1 == 3)&&(setDice2 == 1)){
        System.out.println("Easy Four");
      }
      else if ((setDice1 == 4)&&(setDice2 == 1)){
        System.out.println("Fever Five");
      }
      else if ((setDice1 == 5)&&(setDice2 == 1)){
        System.out.println("Easy Six");
      }
      else if ((setDice1 == 6)&&(setDice2 == 1)){
        System.out.println("Seven Out");
      }
      else if ((setDice1 == 2)&&(setDice2 == 2)){
        System.out.println("Hard Four");
      }
      else if ((setDice1 == 3)&&(setDice2 == 2)){
        System.out.println("Fever Five");
      }
      else if ((setDice1 == 4)&&(setDice2 == 2)){
        System.out.println("Easy Six");
      }  
      else if ((setDice1 == 5)&&(setDice2 == 2)){
        System.out.println("Seven Out");
      }
      else if ((setDice1 == 6)&&(setDice2 == 2)){
        System.out.println("Easy Eight");
      }
      else if ((setDice1 == 3)&&(setDice2 == 3)){
        System.out.println("Hard Six");
      }
      else if ((setDice1 == 4)&&(setDice2 == 3)){
        System.out.println("Seven Out");
      }
      else if ((setDice1 == 5)&&(setDice2 == 3)){
        System.out.println("Easy Eight");
      }
      else if ((setDice1 == 6)&&(setDice2 == 3)){
        System.out.println("Nine");
      }  
      else if ((setDice1 == 4)&&(setDice2 == 4)){
        System.out.println("Hard Eight");
      }
      else if ((setDice1 == 5)&&(setDice2 == 4)){
        System.out.println("Nine");
      }
      else if ((setDice1 == 6)&&(setDice2 == 4)){
        System.out.println("Easy Ten");
      }
      else if ((setDice1 == 5)&&(setDice2 == 5)){
        System.out.println("Hard Ten");
      }
      else if ((setDice1 == 6)&&(setDice2 == 5)){
        System.out.println("Yo-leven");
      }
      else if ((setDice1 == 6)&&(setDice2 == 6)){
        System.out.println("Boxcars");
      }
      else {
        System.out.println("Invalid");
      }
  }
  else {
    System.out.println("Invalid Response");
  } //end of if statements
  
 
  } //end of class
} //end of main method

  
    
  
  
  
  