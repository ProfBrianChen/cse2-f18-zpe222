//Zack Elliott, Zach Badrawi, Charlie Shen
//Venmo Activity Class 2
//Sept 3
//CSE 2 - 210
public class VenmoVariables{
  
  public static void main(String args[]){
    ///Venmo Variables
    int phoneNumber;
    //Receiver's Phone Number
    int senderBankAcct;
    //Sender's bank account
    int receiverBankAcct;
    //Receiver's bank account
    double acctBalance;
    //Initial account balance of sender
    double transactionAmount;
    //Actual amount of transaction
    int time;
    //Time of transaction
    String notification;
    //notification after the transaction
    
    phoneNumber = 712456789;
    senderBankAcct = 12345;
    receiverBankAcct = 15678;
    acctBalance = 3000;
    transactionAmount = 100;
    time = 1342;
    notification = "Success!";
    
    System.out.println("Receiver Phone Number: "+phoneNumber);
    System.out.println("Sender Bank Account: "+senderBankAcct);
    System.out.println("Initial Balance of Sender Bank Account: "+acctBalance);
    System.out.println("Receiver Bank Account: "+receiverBankAcct);
    System.out.println("Transaction Amount: "+transactionAmount);
    System.out.println("Transaction Time: "+time);
    System.out.println(notification);
    
  
    
    
      
      
  }
}