//Zack Elliott
//CSE 2 - lab06
//October 13
//lab06 - printing out pyramid pattern D

import java.util.Scanner; //importing the scanner
  
public class PatternD{ 
  //main method required for every java program
  public static void main(String args[]){
    
    int numRows = 0; //variable for number of pyramid rows
    int input = 0; //variable for user input
    String junk; //variable for junk, which clears out incorrect input
    boolean correctInput = false; //variable for the boolean variable of correct input
    Scanner scanner = new Scanner(System.in); //initializing the scanner
    
    while (!correctInput){ //while the variable correct input is not true
      System.out.println("Enter an integer from 1-10 for the number of rows in the pyramid: ");
      if (scanner.hasNextInt()){ //if the scanner picks up an integer from user input
        input = scanner.nextInt(); //input is equal to the integer entered by the user
        correctInput = true; //correct input becomes true
        if (input<1 || input>10){ //if the input is not between 1-10
          System.out.println("You did not enter a correct integer, try again.");
          correctInput = false; //correctInput becomes false
        }
      }
      else { //if the scanner does NOT pick up an integer from the user input
        junk = scanner.next(); //clear out the input
        System.out.println("You did not enter a correct integer, try again.");
      }
    }

    for (int i = input; i>=1; i--){
      //the for loop for the number of pyramid rows that will be printed out
      //using integers j and i within the for loops
      for (int j = input; j>=1; j--){
        if (i>=j){ //only prints out numbers for the pyramid is i is greater than or equal to j
          System.out.print(j+" ");
        }
      }
      System.out.println(); //need to start on a new line for each row of the pyramid
    }
      
      
    }
  //end of class and main method
}