//Zack Elliott
//CSE 2 - lab06
//October 10
//lab06 - printing out pyramid pattern A

import java.util.Scanner; //importing the scanner
  
public class PatternA{ 
  //main method required for every java program
  public static void main(String args[]){
    
    int numRows = 0; //variable for number of pyramid rows
    int input = 0; //variable for user input
    String junk; //variable for junk, which clears out incorrect input
    boolean correctInput = false; //variable for the boolean variable of correct input
    Scanner scanner = new Scanner(System.in); //initializing the scanner
    
    while (!correctInput){ //while the variable correct input is not true
      System.out.println("Enter an integer from 1-10 for the number of rows in the pyramid: ");
      if (scanner.hasNextInt()){ //if the scanner picks up an integer from user input
        input = scanner.nextInt(); //input is equal to the integer entered by the user
        correctInput = true; //correct input becomes true
        if (input<1 || input>10){ //if the input is not between 1-10
          System.out.println("You did not enter a correct integer, try again.");
          correctInput = false; //correctInput becomes false
        }
      }
      else { //if the scanner does NOT pick up an integer from the user input
        junk = scanner.next(); //clear out the input
        System.out.println("You did not enter a correct integer, try again.");
      }
    }

    for (numRows = 1; numRows<=input; numRows++){
      //the for loop for the number of pyramid rows that will be printed out
      //2 for loops, relating j and numRows
      for (int j =1 ; j<=numRows; j++){
        System.out.print(j+" ");
      }
      System.out.println();
    }
      
      
    }
  //end of class and main method
}